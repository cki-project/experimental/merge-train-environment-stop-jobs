"""Python example project entry point."""

import sys
import time

from cki_lib import logger
from cki_lib import session

# outside of __main__, __file__ can be used
LOGGER = logger.get_logger('cki.template.example')
SESSION = session.get_session('cki.template.example')


def log_ip():
    """Log the IP address."""
    response = SESSION.get('https://ipv4.icanhazip.com')
    response.raise_for_status()
    ip_address = response.text
    LOGGER.info('Hello from %s', ip_address)


def main(_):
    """Handle CLI main entry point."""
    while True:
        log_ip()
        time.sleep(60)


if __name__ == '__main__':
    main(sys.argv[1:])
